---
title: "Roadmap 📅"
description: Grey Software's 2021 Roadmap
category: Info
position: 3
---

At the start of 2021, Grey software's team began to work on creating a brand new vision and strategy for the coming year. We called this effort V3. You can read more about this milestone [here](https://gitlab.com/grey-software/org/-/milestones/2).

After scaffolding V3, we moved towards cleaning up our company with our V3.1 Milestone. You can read more about this milestone [here](https://gitlab.com/grey-software/org/-/milestones/3). This milestone is currently underway and has a target date of 7th May.

## 🎯 Be a model organization for openness and transparency

### Why is this important?

As a company that prides itself on openness and transparency, it is important for us to be a model organization in the space by staying at the forefront of transparent organization practices.

### Is this urgent?

Yes, because developing this infrastructure will help develop our brand and facilitate important organization processes that we need to be running and are unfortunately falling behind on.

## 🎯 Be a global OSS Education thought leader

### Why is this important?

**A thought leader has a positive reputation for helping others with their knowledge and insight.**

Our organization was founded on the principle of helping eager students from around the Internet to learn software development from applied OSS experience.

Being associated with skill development and opportunity creation on the Internet will help us develop a positive relationship with the public.

### Is this urgent?

Yes, because the industry is ripe for OSS education companies to come in and do something great. We would like to establish ourselves as one of the thought leaders in the industry alongside MLH, Code4Cause, and others.

## 🎯 Distribute premium versions of our free software to Sponsors

### Why is this important?

Our organization wants to provide the public with free software that they can trust, love, and learn from. To do this sustainably, we have a sponsorships program on Github that we used to generate revenue for the organization. In return for their support, we want to provide them premium versions of our free software to let them know how grateful we are for their financial backing of our organization and their belief in the quality of our software.

### Is this urgent?

Yes, because early on we were calling on peoples' good faith to drive the growth of this organization, **but now with close to a year under our belt,** we must build a bridge between receiving good faith and returning life-enhancing value. Not prioritizing this means we are not staying true to our promise of delivering great OSS.
